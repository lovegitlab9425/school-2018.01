from .keyvalue import KeyValueStorage

from queue import PriorityQueue


class NonVolatileQueue:
    def __init__(self, rootdir="", cache_size=10**5):
        self._store = KeyValueStorage(rootdir)
        self._queue = PriorityQueue(maxsize=cache_size)

        self._cache = {}

    def contains(self, key):
        return self._store.exists(key)

    def dispose(self, key):
        self._store.remove(key)
        self._cache.pop(key, None)

    def put(self, key, priority, item):
        self._store.persist(key, (priority, item))

        if key in self._cache:
            if self._cache[key] is None:
                # item has been dequeued.
                # there is not much we can do with it now.
                return
        elif self._enqueue(priority, key):
            # queue is full, use fs only (no cache)
            return

        # update volatile cache if:
        # - item is still enqueued
        # - a new item was added to non-volatile storage
        self._cache[key] = item

    def take(self):
        key = self._take_key()
        if key is None:
            return None

        item = self._get_item(key)
        self._cache[key] = None
        return key, item

    def _enqueue(self, priority, key):
        try:
            self._queue.put_nowait((priority, key))
            return False
        except:
            return True

    def _dequeue(self):
        try:
            return self._queue.get_nowait()
        except:
            return None

    def _get_item(self, key):
        item = self._cache.get(key, None)
        if item is not None:
            return item

        _, item = self._store.load(key) or (None, None)
        return item

    def _take_key(self, populate_queue=True):
        _, key = self._dequeue() or (None, None)
        if key is not None:
            return key

        if populate_queue:
            self._populate_queue()
            return self._take_key(populate_queue=False)
        return None

    # Fill the queue with data from fs.
    # Returns last value
    def _populate_queue(self):
        for key, data in self._store.items():
            priority, item = data
            self.put(key, priority, item)
